﻿using Orion;
using Orion.Storage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    private void Awake()
    {
        AppHost.CreateDefault()
            .AddStorageService(ConfigureStorageService)
            .AddRunAction(LogUserData)
            .AddRunAction(LogUserDataSingleton)
            .Run();
    }

    private void ConfigureStorageService(StorageServiceConfig config)
    {
        // create json file provider
        var provider = new JsonFileProvider<UserData>("userdata.json");
        provider.LoadOrCreate();

        // add data provider to autosave
        StorageService.Instance.AddDataProvider("userdata", provider, DataProviderOptions.Autosave);

        // do same with singleton
        UserDataSingleton.DataProvider = new JsonFileProvider<UserDataSingleton>("userdata-singleton.json");
        UserDataSingleton.DataProvider.LoadOrCreate();
        StorageService.Instance.AddDataProvider("userdata-singleton", UserDataSingleton.DataProvider, DataProviderOptions.Autosave);
    }

    private void LogUserData(IAppHost appHost)
    {
        var provider = StorageService.Instance.GetDataProvider<UserData>("userdata");
        var user = provider.Data;

        user.Name = "New user";
        user.Age = 25;
        Debug.Log(user);
    }

    private void LogUserDataSingleton(IAppHost appHost)
    {
        var user = UserDataSingleton.Instance;

        user.Name = "New user";
        user.Age = 25;
        Debug.Log(user);
    }
}
