﻿using Newtonsoft.Json;
using Orion.Storage;
using System;

public class UserData
{
    [JsonProperty] private string _id;
    [JsonIgnore] public string Id => !string.IsNullOrEmpty(_id) ? _id : _id = Guid.NewGuid().ToString();

    public string Name { get; set; }
    public int Age { get; set; }

    public override string ToString()
    {
        return $"Id = {Id}, Name = {Name}, Age = {Age}";
    }
}


public class UserDataSingleton : UserData
{
    public static IDataProvider<UserDataSingleton> DataProvider { get; set; }

    public static UserDataSingleton Instance => DataProvider?.Data;
}
