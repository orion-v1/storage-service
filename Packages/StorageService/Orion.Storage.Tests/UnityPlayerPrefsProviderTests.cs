﻿using NUnit.Framework;
using Orion.Storage;
using System;

namespace Origin.Storage.Tests
{
    [TestFixture]
    public class UnityPlayerPrefsProviderTests
    {
        public const string Key = "testData";

        [Test]
        public void Save_Json()
        {
            var provider = new UnityPlayerPrefsProvider<TestData>(Key, new NewtonsoftJsonSerializer());

            if (provider.Exists()) provider.Delete();

            TestData data = null;
            try
            {
                provider.Data = new TestData { Name = "User1", Score = 100 };
                provider.Save();
                provider.Clear();

                provider.Load();
                data = provider.Data;
            }
            finally
            {
                provider.Delete();
            }

            Assert.That(data, Is.Not.Null);
            Assert.That(data.Name, Is.EqualTo("User1"));
            Assert.That(data.Score, Is.EqualTo(100));
        }


        [Serializable]
        private class TestData
        {
            public string Name { get; set; }
            public int Score { get; set; }
        }
    }
}
