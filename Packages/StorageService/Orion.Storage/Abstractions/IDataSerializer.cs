﻿using System.IO;

namespace Orion.Storage
{
    public interface IDataSerializer
    {
        T Deserialize<T>(Stream stream, bool leaveOpen = false);
        void Serialize(Stream stream, object data, bool leaveOpen = false);
    }
}
