﻿namespace Orion.Storage
{
    public interface IPathResolver
    {
        string GetPath(string filename);
    }
}
