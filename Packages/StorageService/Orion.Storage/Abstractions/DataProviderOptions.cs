﻿using System;

namespace Orion.Storage
{
    [Flags]
    public enum DataProviderOptions
    {
        None = 0,
        Autosave = 1,
    }
}
