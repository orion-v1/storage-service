﻿using System.IO;

namespace Orion.Storage
{
    public interface IFileSystemStorage
    {
        string ResolvePath(string path);
        bool FileExists(string path);
        Stream CreateFile(string path);
        Stream OpenFile(string path);
        void DeleteFile(string path);
        void MoveFile(string src, string dst, bool overwrite = false);
    }
}
