﻿using System;

namespace Orion.Storage
{
    [Flags]
    public enum RuntimeMode
    {
        Disabled = 0,
        Player = 1,
        Editor = 2
    }
}
