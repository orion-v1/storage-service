﻿namespace Orion.Storage
{
    public interface IDataProvider
    {
        bool HasData { get; }

        bool Exists();
        void Load();
        void Save();
        void Delete();

        void Create();
        void LoadOrCreate();
        void Clear();
    }


    public interface IDataProvider<out T> : IDataProvider
    {
        T Data { get; }
    }

    public interface IDataProviderMutable<in T> : IDataProvider
    {
        T Data { set; }
    }
}
