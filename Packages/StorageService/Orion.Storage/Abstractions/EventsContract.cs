﻿namespace Orion.Storage.Events
{
    public interface IAfterLoadedHandler
    {
        void OnAfterLoad();
    }

    public interface IBeforeSaveHandler
    {
        void OnBeforeSave();
    }
}
