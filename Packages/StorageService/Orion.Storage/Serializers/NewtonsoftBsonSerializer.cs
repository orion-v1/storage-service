﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace Orion.Storage
{
    public class NewtonsoftBsonSerializer : IDataSerializer
    {
        private readonly JsonSerializer _serializer;

        public NewtonsoftBsonSerializer()
        {
            _serializer = JsonSerializer.CreateDefault();
        }

        public NewtonsoftBsonSerializer(JsonSerializerSettings settings)
        {
            _serializer = JsonSerializer.CreateDefault(settings);
        }

        public void Serialize(Stream stream, object data, bool leaveOpen = false)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            using (var bsonWriter = new BsonWriter(stream))
            {
                bsonWriter.CloseOutput = !leaveOpen;
                bsonWriter.DateTimeKindHandling = System.DateTimeKind.Utc;
                _serializer.Serialize(bsonWriter, data);
            }
#pragma warning restore CS0618 // Type or member is obsolete
        }

        public T Deserialize<T>(Stream stream, bool leaveOpen = false)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            using (var jsonReader = new BsonReader(stream))
            {
                jsonReader.CloseInput = !leaveOpen;
                jsonReader.DateTimeKindHandling = System.DateTimeKind.Utc;
                return _serializer.Deserialize<T>(jsonReader);
            }
#pragma warning restore CS0618 // Type or member is obsolete
        }
    }
}
