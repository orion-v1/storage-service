﻿using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace Orion.Storage
{
    public class NewtonsoftJsonSerializer : IDataSerializer
    {
        private const int DefaultBufferSize = 1024;
        private readonly static UTF8Encoding DefaultEncoding = new UTF8Encoding(false, true); // UTF8 no BOM
        private readonly JsonSerializer _serializer;

        public NewtonsoftJsonSerializer()
        {
            _serializer = JsonSerializer.CreateDefault();
        }

        public NewtonsoftJsonSerializer(JsonSerializerSettings settings)
        {
            _serializer = JsonSerializer.CreateDefault(settings);
        }

        public void Serialize(Stream stream, object data, bool leaveOpen = false)
        {
            if (leaveOpen)
            {
                // HOTFIX: StreamWriter has no constructor with (stream, leaveOpen).
                // Because of this, agruments DefaultBufferSize, DefaultEncoding had to be duplicated from dotnet
                using (var textWriter = new StreamWriter(stream, DefaultEncoding, DefaultBufferSize, true))
                using (var jsonWriter = new JsonTextWriter(textWriter))
                {
                    jsonWriter.CloseOutput = false;
                    _serializer.Serialize(jsonWriter, data);
                }
            }
            else
            {
                using (var textWriter = new StreamWriter(stream))
                using (var jsonWriter = new JsonTextWriter(textWriter))
                    _serializer.Serialize(jsonWriter, data);
            }
        }

        public T Deserialize<T>(Stream stream, bool leaveOpen = false)
        {
            if (leaveOpen)
            {
                // HOTFIX: StreamWriter has no constructor with (stream, leaveOpen).
                // Because of this, agruments DefaultBufferSize, DefaultEncoding had to be duplicated from dotnet
                using (var textReader = new StreamReader(stream, DefaultEncoding, true, DefaultBufferSize, true))
                using (var jsonReader = new JsonTextReader(textReader))
                {
                    jsonReader.CloseInput = false;
                    return _serializer.Deserialize<T>(jsonReader);
                }
            }
            else
            {
                using (var textReader = new StreamReader(stream))
                using (var jsonReader = new JsonTextReader(textReader))
                    return _serializer.Deserialize<T>(jsonReader);
            }

        }
    }
}
