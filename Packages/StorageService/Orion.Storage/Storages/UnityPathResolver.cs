﻿using System;
using System.IO;
using UnityEngine;

namespace Orion.Storage
{
    public class UnityPathResolver : IPathResolver
    {
        private const string RootDirectoryName = "AppData";

        private readonly string _root;

        public UnityPathResolver(
            UnityPathResolverRoot root = UnityPathResolverRoot.PersistentDataPath,
            string isolatedDirectoryName = null)
        {
            string rootPath;
            switch (root)
            {
                case UnityPathResolverRoot.PersistentDataPath:
                    rootPath = Application.persistentDataPath;
                    break;

                case UnityPathResolverRoot.DataPath:
#if UNITY_EDITOR
                    rootPath = Path.Combine(
                        Path.GetDirectoryName(Application.dataPath),
                        RootDirectoryName);
#else
                    rootPath = Application.dataPath;
#endif
                    break;

                default: throw new NotImplementedException(root.ToString());
            }

            if (!string.IsNullOrEmpty(isolatedDirectoryName))
                rootPath = Path.Combine(rootPath, isolatedDirectoryName);

            _root = FixSlashes(rootPath);
        }

        private string FixSlashes(string path) => path.Replace('\\', '/');

        public string GetPath(string filename) => FixSlashes(Path.Combine(_root, filename));
    }


    public enum UnityPathResolverRoot
    {
        PersistentDataPath = 0,
        DataPath
    }
}
