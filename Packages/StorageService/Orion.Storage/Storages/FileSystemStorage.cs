﻿using System.IO;

namespace Orion.Storage
{
    public class FileSystemStorage : IFileSystemStorage
    {
        private readonly IPathResolver _pathResolver;

        public FileSystemStorage(IPathResolver pathResolver)
        {
            _pathResolver = pathResolver;
        }

        public string ResolvePath(string path)
        {
            return Path.IsPathRooted(path)
                ? path
                : _pathResolver?.GetPath(path) ?? path;
        }

        public bool FileExists(string path)
        {
            path = ResolvePath(path);
            return File.Exists(path);
        }

        public Stream CreateFile(string path)
        {
            path = ResolvePath(path);
            CreateDirectoryForFile(path);
            return File.Create(path);
        }

        public Stream OpenFile(string path)
        {
            path = ResolvePath(path);
            return File.OpenRead(path);
        }

        public void DeleteFile(string path)
        {
            path = ResolvePath(path);
            File.Delete(path);
        }

        public void MoveFile(string src, string dst, bool overwrite = false)
        {
            src = ResolvePath(src);
            dst = ResolvePath(dst);

            if (overwrite && File.Exists(src) && File.Exists(dst))
                File.Delete(dst);

            CreateDirectoryForFile(dst);
            File.Move(src, dst);
        }

        private void CreateDirectoryForFile(string path)
        {
            try
            {
                var dir = Path.GetDirectoryName(path);
                if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
            catch
            {
                // attemp to suppress exception on android (not reproducible)
                // UnauthorizedAccessException: Access to the path /storage/emulated/0/Android/data/<package-id>
            }
        }
    }
}
