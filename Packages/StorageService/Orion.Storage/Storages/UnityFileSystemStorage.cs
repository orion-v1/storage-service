﻿namespace Orion.Storage
{
    public class UnityFileSystemStorage : FileSystemStorage
    {
        public UnityFileSystemStorage(
            UnityPathResolverRoot root = UnityPathResolverRoot.PersistentDataPath,
            string isolatedDirectoryName = null) : base(new UnityPathResolver(root, isolatedDirectoryName))
        {
        }
    }
}
