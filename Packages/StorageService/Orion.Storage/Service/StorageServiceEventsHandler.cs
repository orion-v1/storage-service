﻿using System;
using UnityEngine;

namespace Orion.Storage
{
    internal class StorageServiceEventsHandler : MonoBehaviour
    {
        private int _lastSaveFrame = -1;

        public event Action SaveAll;


        private void OnApplicationPause(bool pause)
        {
            if (!pause) return;
            RaiseSaveAll();
        }

        private void OnDestroy() => RaiseSaveAll();

        private void RaiseSaveAll()
        {
            if (_lastSaveFrame == Time.frameCount) return;
            _lastSaveFrame = Time.frameCount;
            SaveAll.Invoke();
        }
    }
}
