﻿using System;

namespace Orion.Storage
{
    public static class StorageServiceExtensions
    {
        public static IAppHostBuilder AddStorageService(
            this IAppHostBuilder appHostBuilder,
            StorageServiceConfig config,
            Action<StorageServiceConfig> configure)
            => appHostBuilder.AddModule(new StorageServiceModule(config, configure));

        public static IAppHostBuilder AddStorageService(this IAppHostBuilder appHostBuilder)
            => AddStorageService(appHostBuilder, null, null);

        public static IAppHostBuilder AddStorageService(this IAppHostBuilder appHostBuilder, StorageServiceConfig config)
            => AddStorageService(appHostBuilder, config, null);

        public static IAppHostBuilder AddStorageService(this IAppHostBuilder appHostBuilder, Action<StorageServiceConfig> configure)
            => AddStorageService(appHostBuilder, null, configure);
    }
}
