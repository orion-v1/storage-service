﻿using System;

namespace Orion.Storage
{
    internal class StorageServiceModule : IAppHostModule
    {
        private StorageServiceConfig _config;
        private Action<StorageServiceConfig> _configAction;

        public StorageServiceModule(StorageServiceConfig config, Action<StorageServiceConfig> configAction)
        {
            _config = config;
            _configAction = configAction;
        }

        public void AttachToAppHostBuilder(IAppHostBuilder appHostBuilder)
        {
            appHostBuilder.AddRunAction(AppHostLayer.Environment, "Storage Service", Run);
        }

        private void Run(IAppHost appHost)
        {
            if (_config == null)
                _config = new StorageServiceConfig();

            _configAction?.Invoke(_config);
            StorageService.Instance.Run(_config);
        }
    }
}
