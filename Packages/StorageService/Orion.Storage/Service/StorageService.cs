using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Orion.Storage
{
    public class StorageService
    {
        private bool _ready = false;
        private Dictionary<string, DataProviderEntity> _providers = new Dictionary<string, DataProviderEntity>();

        public event Action AutoSaveStarted;
        public event Action AutoSaveCompleted;

        #region Instancing

        public static StorageService Instance { get; } = new StorageService();

        private StorageService() { }

        #endregion

        #region Configure and Run

        internal void Run(StorageServiceConfig config)
        {
            var eventHandler = AppHost.Instance.GameObject.CreateChild<StorageServiceEventsHandler>();
            eventHandler.SaveAll += SaveAll;

            _ready = true;
        }

        #endregion

        #region Providers under control

        public bool ContainsDataProvider(string id) => _providers.ContainsKey(id);

        public bool ContainsDataProvider(IDataProvider dataProvider) => _providers.Any(p => p.Value.DataProvider == dataProvider);

        public IDataProvider FindDataProvider(string id) => _providers.TryGetValue(id, out var entity)
            ? entity.DataProvider
            : null;

        public IDataProvider<T> FindDataProvider<T>(string id) => _providers.TryGetValue(id, out var entity)
            ? (IDataProvider<T>)entity.DataProvider
            : null;

        public IDataProvider GetDataProvider(string id) => _providers.TryGetValue(id, out var entity)
            ? entity.DataProvider
            : throw new KeyNotFoundException($"Data provider with id '{id}' doesn't exist.");

        public IDataProvider<T> GetDataProvider<T>(string id) => _providers.TryGetValue(id, out var entity)
            ? (IDataProvider<T>)entity.DataProvider
            : throw new KeyNotFoundException($"Data provider with id '{id}' doesn't exist.");

        public void AddDataProvider(string id, IDataProvider dataProvider, DataProviderOptions options = DataProviderOptions.None) =>
            _providers.Add(id, new DataProviderEntity(dataProvider, options));

        public bool RemoveDataProvider(string id) =>
            _providers.Remove(id);

        public bool RemoveDataProvider(IDataProvider dataProvider)
        {
            var keys = _providers
                .Where(p => p.Value.DataProvider == dataProvider)
                .Select(p => p.Key)
                .ToList();

            bool rs = false;
            foreach (var key in keys)
                rs |= _providers.Remove(key);

            return rs;
        }

        #endregion

        #region Save

        public void SaveAll()
        {
            if (!_ready) return;

            var uniqueProviders = new HashSet<IDataProvider>(_providers
                .Where(p => p.Value.AutosaveEnabled)
                .Select(p => p.Value.DataProvider));

            SafeCall(AutoSaveStarted, "An error occurred while processing event AutoSaveStarted");
            foreach (var provider in uniqueProviders)
                Save(provider);
            SafeCall(AutoSaveCompleted, "An error occurred while processing event AutoSaveCompleted");
        }

        private void Save(IDataProvider dataProvider)
        {
            if (!dataProvider.HasData) return;

            try
            {
                dataProvider.Save();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void SafeCall(Action action, string exceptionMessage)
        {
            if (action == null) return;

            try
            {
                AutoSaveStarted.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError($"{exceptionMessage}\n{e}");
            }
        }

        #endregion


        private readonly struct DataProviderEntity
        {
            public IDataProvider DataProvider { get; }
            public DataProviderOptions DataProviderOptions { get; }
            public bool AutosaveEnabled => (DataProviderOptions & DataProviderOptions.Autosave) != 0;

            public DataProviderEntity(IDataProvider dataProvider, DataProviderOptions options)
            {
                DataProvider = dataProvider;
                DataProviderOptions = options;
            }
        }
    }
}
