﻿using Orion.Storage.Events;
using System;
using System.IO;
using UnityEngine;

namespace Orion.Storage
{
    public class UnityPlayerPrefsProvider<T> : IDataProvider<T>, IDataProviderMutable<T> where T : class
    {
        private const int DefaultBufferSize = 1024;
        private readonly IDataSerializer _serializer;

        public string Key { get; set; }

        public T Data { get; set; }

        public bool HasData => Data != null;


        public UnityPlayerPrefsProvider(
            string key,
            IDataSerializer serializer)
        {
            Key = key;
            _serializer = serializer;
        }

        public bool Exists() => PlayerPrefs.HasKey(Key);

        public void Load()
        {
            T data;

            var value = PlayerPrefs.GetString(Key);
            using (var stream = new MemoryStream(DefaultBufferSize))
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(value);
                writer.Flush();

                stream.Position = 0;
                data = _serializer.Deserialize<T>(stream);

                if (data == null)
                    throw new InvalidDataException($"Key '{Key}' has invalid data.");
            }

            Data = data;
            OnAfterLoad();
        }

        public void Save()
        {
            if (Data == null) return;

            OnBeforeSave();

            // serialize
            using (var stream = new MemoryStream(DefaultBufferSize))
            using (var reader = new StreamReader(stream))
            {
                _serializer.Serialize(stream, Data, true);

                stream.Position = 0;
                PlayerPrefs.SetString(Key, reader.ReadToEnd());
            }

            PlayerPrefs.Save();
        }

        public void Delete()
        {
            PlayerPrefs.DeleteKey(Key);
        }

        public void Create()
        {
            Data = Activator.CreateInstance<T>();
        }

        public void LoadOrCreate()
        {
            bool loaded = false;

            if (Exists())
            {
                try
                {
                    Load();
                    loaded = true;
                }
                catch (Exception e)
                {
                    Debug.Log($"Failed to load data from key '{Key}'. Create new instead.");
                    Debug.LogError(e);
                }
            }

            if (!loaded) Create();
        }

        public void Clear()
        {
            Data = null;
        }


        protected virtual void OnAfterLoad()
        {
            if (Data is IAfterLoadedHandler hdl) hdl.OnAfterLoad();
        }

        protected virtual void OnBeforeSave()
        {
            if (Data is IBeforeSaveHandler hdl) hdl.OnBeforeSave();
        }
    }
}
