﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;

namespace Orion.Storage
{
    public class JsonFileProvider<T> : FileProvider<T> where T : class, new()
    {
        private readonly RuntimeMode _useGzip;

        public JsonFileProvider(
            string path,
            RuntimeMode useGzip = RuntimeMode.Disabled,
            RuntimeMode useBson = RuntimeMode.Disabled
            ) : base(path, GetStorage(), GetSerializer(useBson))
        {
            _useGzip = useGzip;
        }

        private static IFileSystemStorage GetStorage()
        {
#if UNITY_EDITOR
            return new UnityFileSystemStorage(UnityPathResolverRoot.DataPath);
#else
            return new UnityFileSystemStorage();
#endif
        }

        private static IDataSerializer GetSerializer(RuntimeMode useBson)
        {
#if UNITY_EDITOR

            if ((useBson & RuntimeMode.Editor) > 0)
                return new NewtonsoftBsonSerializer();
            else
            {
                var settings = new JsonSerializerSettings();
                settings.Formatting = Formatting.Indented;
                return new NewtonsoftJsonSerializer(settings);
            }
#else
            if ((useBson & RuntimeMode.Player) > 0)
                return new NewtonsoftBsonSerializer();
            else
                return new NewtonsoftJsonSerializer();
#endif
        }

        protected override Stream ModifyLoadStream(Stream stream)
        {
            stream = base.ModifyLoadStream(stream);

#if UNITY_EDITOR
            if ((_useGzip & RuntimeMode.Editor) > 0)
                stream = new GZipStream(stream, CompressionMode.Decompress);
#else
            if ((_useGzip & RuntimeMode.Player) > 0)
                stream = new GZipStream(stream, CompressionMode.Decompress);
#endif

            return stream;
        }

        protected override Stream ModifySaveStream(Stream stream)
        {
            stream = base.ModifySaveStream(stream);

#if UNITY_EDITOR
            if ((_useGzip & RuntimeMode.Editor) > 0)
                stream = new GZipStream(stream, CompressionMode.Compress);
#else
            if ((_useGzip & RuntimeMode.Player) > 0)
                stream = new GZipStream(stream, CompressionMode.Compress);
#endif

            return stream;
        }
    }
}
