﻿using Orion.Storage.Events;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using IOPath = System.IO.Path;

namespace Orion.Storage
{
    public class FileProvider<T> : IDataProvider<T>, IDataProviderMutable<T> where T : class
    {
        private readonly IFileSystemStorage _storage;
        private readonly IDataSerializer _serializer;

        public string Path { get; set; }

        public T Data { get; set; }

        public bool HasData => Data != null;

        public IFileSystemStorage Storage => _storage;


        public FileProvider(
            string path,
            IFileSystemStorage storage,
            IDataSerializer serializer)
        {
            Path = path;
            _storage = storage;
            _serializer = serializer;
        }

        private string GetPath(StateType state)
        {
            switch (state)
            {
                case StateType.Stable: return Path;
                case StateType.Backup: return AddSuffix(Path, ".bak");
                case StateType.Temp: return AddSuffix(Path, ".tmp");
                default:
                    throw new NotImplementedException(state.ToString());
            }


            string AddSuffix(string path, string suffix)
            {
                var ext = IOPath.GetExtension(path);
                if (string.IsNullOrEmpty(ext))
                    return path + suffix;

                return path.Remove(path.Length - suffix.Length - 1) + suffix + ext;
            }
        }

        public string GetAbsolutePath() => _storage.ResolvePath(Path);

        #region Exists

        public bool Exists() =>
            Exists(StateType.Stable) ||
            Exists(StateType.Backup);

        private bool Exists(StateType state)
        {
            var path = GetPath(state);
            return _storage.FileExists(path);
        }

        #endregion

        #region Load

        public void Load()
        {
            T data;
            var exceptions = new List<Exception>(2);

            bool rs =
                TryLoad(StateType.Stable, out data, exceptions) ||
                TryLoad(StateType.Backup, out data, exceptions);

            if (rs)
            {
                Data = data;
                OnAfterLoad();
            }
            else if (exceptions.Count != 0)
            {
                if (exceptions.Count > 1)
                    throw new AggregateException(exceptions);
                throw exceptions[0];
            }
            else
            {
                var path = GetPath(StateType.Stable);
                throw new FileNotFoundException("File not found.", path);
            }
        }

        private bool TryLoad(StateType state, out T data, List<Exception> exceptions)
        {
            data = null;
            var path = GetPath(state);
            if (!_storage.FileExists(path)) return false;

            try
            {
                using (var stream = _storage.OpenFile(path))
                using (var modifiedStream = ModifyLoadStream(stream))
                    data = _serializer.Deserialize<T>(modifiedStream);

                if (data == null)
                    throw new InvalidDataException($"File '{path}' has invalid data.");

                return true;
            }
            catch (Exception ex)
            {
                exceptions.Add(ex);
                return false;
            }
        }

        protected virtual Stream ModifyLoadStream(Stream stream) => stream;

        #endregion

        #region Save

        public void Save()
        {
            if (Data == null) return;

            OnBeforeSave();

            // serialize
            var tempPath = GetPath(StateType.Temp);
            using (var stream = _storage.CreateFile(tempPath))
            using (var modifiedStream = ModifySaveStream(stream))
                _serializer.Serialize(modifiedStream, Data);

            // swap files
            var stablePath = GetPath(StateType.Stable);
            var backupPath = GetPath(StateType.Backup);
            MoveFile(stablePath, backupPath);
            MoveFile(tempPath, stablePath);
        }

        protected virtual Stream ModifySaveStream(Stream stream) => stream;

        private void MoveFile(string src, string dst)
        {
            if (_storage.FileExists(src))
                _storage.MoveFile(src, dst, true);
        }

        #endregion

        #region Delete

        public void Delete()
        {
            Delete(GetPath(StateType.Stable));
            Delete(GetPath(StateType.Backup));
            Delete(GetPath(StateType.Temp));
        }

        private void Delete(string path)
        {
            if (_storage.FileExists(path))
                _storage.DeleteFile(path);
        }

        #endregion

        #region Create & Clear

        public void Create()
        {
            Data = Activator.CreateInstance<T>();
        }

        public void LoadOrCreate()
        {
            bool loaded = false;

            if (Exists())
            {
                try
                {
                    Load();
                    loaded = true;
                }
                catch (Exception e)
                {
                    Debug.Log($"Failed to load file '{Path}'. Create new instead.");
                    Debug.LogError(e);
                }
            }

            if (!loaded) Create();
        }

        public void Clear()
        {
            Data = null;
        }

        #endregion


        protected virtual void OnAfterLoad()
        {
            if (Data is IAfterLoadedHandler hdl) hdl.OnAfterLoad();
        }

        protected virtual void OnBeforeSave()
        {
            if (Data is IBeforeSaveHandler hdl) hdl.OnBeforeSave();
        }


        enum StateType
        {
            Stable = 0,
            Backup,
            Temp
        }
    }
}
